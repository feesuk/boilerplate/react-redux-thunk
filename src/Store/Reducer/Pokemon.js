const initalState = {
  favoritPokemon: [],
  pokemons: [],
};

export default (state = initalState, action) => {
  switch (action.type) {
    case "SET_FAVORIT_POKEMON":
      return {
        ...state,
        favoritPokemon: state.favoritPokemon.concat(action.payload),
      };
    case "GET_POKEMON":
      return {
        ...state,
        pokemons: action.payload,
      };
    default:
      break;
  }
  return state;
};
