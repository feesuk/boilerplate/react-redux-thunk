import axios from "axios";

export const getPokemon = (params) => (dispatch) => {
  const { limit, offset } = params;
  axios
    .get(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`)
    .then((response) => {
      if (response.status === 200) {
        dispatch({
          type: "GET_POKEMON",
          payload: response.data.results,
        });
      }
    })
    .catch((err) => console.log(err));
};

export const setFavoritPokemon = (params) => (dispatch) => {
  dispatch({
    type: "SET_FAVORIT_POKEMON",
    payload: params,
  });
};

/**
 * code diatas sama sepertin kode dibawah ini
export const setFavoritPokemon = (params) => {
  return (dispatch) => {
    dispatch({
      type: "SET_FAVORIT_POKEMON",
      payload: params,
    });
  };
};
 */
